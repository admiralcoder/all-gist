@Test(description = "Checkbox using Xpath Index")
public static void checkIndexBox() {

    System.setProperty
            ("webdriver.chrome.driver", "src/main/driver/chromedriver");
    WebDriver driver = new ChromeDriver();

    // navigate to url
    driver.get("http://the-internet.herokuapp.com/");

    driver.findElement
            (By.xpath("//a[contains(text(),'Checkboxes')]")).click();

    // Scenario 1 = click on checkbox using xpath index
    driver.findElement
            (By.xpath("//input[@type='checkbox'][1]")).click();

}
